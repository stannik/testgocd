package home.yy.ee.ww.qq.controllers;

import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class SampleController {

    @RequestMapping("/")
    String home(Model model) {
        model.addAttribute("foo", "test ttt");
        return "index";
    }
}